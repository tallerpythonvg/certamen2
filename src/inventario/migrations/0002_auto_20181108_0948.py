# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-11-08 12:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventario', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ingresoinventario',
            name='cantidad_producto',
            field=models.PositiveIntegerField(max_length=11),
        ),
    ]
