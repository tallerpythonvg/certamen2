from django.db import models


class IngresoInventario(models.Model):
    id_producto = models.AutoField(primary_key=True, unique=True)
    nombre_producto = models.CharField(max_length=100, blank=False, null=False)
    cantidad_producto = models.PositiveIntegerField(max_length=11)
    pertenencia_primaria = models.CharField(max_length=100)
    pertenencia_secundaria = models.CharField(max_length=100)

    # class Meta:
    #     objects = models.Manager()

    def __str__(self):
        return self.nombre_producto


